<?php get_header()?>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-9 content">
            <h1 class="title-page"><span><?php single_cat_title(); ?><?php post_type_archive_title(); ?></span></h1>
            <?php if(have_posts()) : ?>

                <?php while (have_posts()) : the_post(); ?>

                    <?php

                    /* Include the Post-Format-specific template for the content.

                     * If you want to override this in a child theme, then include a file

                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.

                     */

                    if( get_post_type() == 'product' ) {

                         get_template_part( 'template-parts/content', 'product' );

                    } else  {

                         get_template_part( 'template-parts/content', '' );

                    }

                    ?>

                <?php endwhile; ?>

                <?php if(function_exists('wp_pagenavi')): ?>

                    <?php wp_pagenavi(); ?>

                <?php endif; ?>

            <?php else : ?>

                <?php get_template_part( 'template-parts/content', 'none' ); ?>

            <?php endif; ?>

        </div> 

        <?php get_sidebar();?>

   </div>         

<?php get_footer(); ?>