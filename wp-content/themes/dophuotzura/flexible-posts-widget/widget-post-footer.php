<?php
/**
 * Flexible Posts Widget: Old Default widget template
 * 
 * @since 1.0.0
 *
 * This is the ORIGINAL default template used by the plugin.
 * There is a new default template (default.php) that will be 
 * used by default if no template was specified in a widget.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

echo $before_widget; ?>


<?php if ( !empty($title) )
	echo $before_title . $title . $after_title;

if( $flexible_posts->have_posts() ):
?>
<ul>
	<?php while( $flexible_posts->have_posts() ) : $flexible_posts->the_post(); global $post; ?>
	<li>
		<a class="post-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	      <?php echo kenit_limit_words(get_the_title(), '10'); ?>
	    </a>
    </li>
	<?php endwhile; ?>
</ul>
<?php else: // We have no posts ?>
	<div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>
<?php	
endif; // End have_posts()
	?>

<?php 
echo $after_widget;
