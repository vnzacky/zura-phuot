<?php
/**
 * Flexible Posts Widget: Old Default widget template
 * 
 * @since 1.0.0
 *
 * This is the ORIGINAL default template used by the plugin.
 * There is a new default template (default.php) that will be 
 * used by default if no template was specified in a widget.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

echo $before_widget; ?>
<div class="news">

<?php if ( !empty($title) )
	echo $before_title . $title . $after_title;

if( $flexible_posts->have_posts() ):
?>
	<?php while( $flexible_posts->have_posts() ) : $flexible_posts->the_post(); global $post; ?>
		<div class="row post-list">
		  <div class="col-xs-4 col-sm-4 col-md-5">
		     <a class="post-img"  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		          <?php the_post_thumbnail('medium', array( 'class' => 'img-responsive center-block' ) ); ?>
		      </a>
		  </div>
		  <div class="col-xs-8 col-sm-8 col-md-7">
		    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-title">
		        <?php echo kenit_limit_words(get_the_title(), '5') ?>
		    </a>
		    <span class="post-date"><i class="fa fa-calendar"></i> <?php the_time('F j, Y'); ?> </span>
		  </div>
		</div>
	<?php endwhile; ?>
<?php else: // We have no posts ?>
	<div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>
<?php	
endif; // End have_posts()
	?>
</div>
<?php 
echo $after_widget;
