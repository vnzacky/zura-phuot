<?php
/**
 * Flexible Posts Widget: Old Default widget template
 * 
 * @since 1.0.0
 *
 * This is the ORIGINAL default template used by the plugin.
 * There is a new default template (default.php) that will be 
 * used by default if no template was specified in a widget.
 */

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

echo $before_widget; ?>

<?php if ( !empty($title) )
	echo $before_title . $title . $after_title;?>
<div class="list-content">  
<?php if( $flexible_posts->have_posts() ):
?>
 
              <div class="row"> 
	<?php while( $flexible_posts->have_posts() ) : $flexible_posts->the_post(); global $post; ?>
	<?php
	get_template_part( 'template-parts/content', 'pro' );
	?>
	<?php endwhile; ?>
	</div>
<?php else: ?>
	<div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>

<?php	endif; wp_reset_query(); ?>
</div><!--/list-content-->
<?php 
echo $after_widget;
