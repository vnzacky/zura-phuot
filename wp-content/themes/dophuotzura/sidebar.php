<div class="col-xs-12 col-sm-12 col-md-3">
  <div class="siderbar">
  	<?php dynamic_sidebar('sidebar');?>
    <div class="block">
		<h3 class="title"><span>Dịch vụ</span></h3>
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <?php  $args = array(
            'post_type' => 'post', 
            'showposts' => '4',
            'order' => 'DESC', 
            'orderby' => 'ID',                            
        	); 
        	$news = new WP_Query($args); 
        	$count = 0;
          ?>
          <?php  if ($news->have_posts()) : ?>
                <?php while ($news->have_posts()) : $news->the_post(); $count ++?>
                	<div class="panel panel-primary">
	              <div class="panel-heading" role="tab" id="heading-<?php echo $count; ?>">
	                <h4 class="panel-title">
	                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $count; ?>" aria-expanded="true" aria-controls="collapse-<?php echo $count; ?>">
	                      <?php the_title(); ?>
	                  </a>
	                </h4>
	              </div>
	              <div id="collapse-<?php echo $count; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $count; ?>">
	                <div class="panel-body">
	                    Chi tiết: <?php echo kenit_limit_words(get_the_excerpt(), '66'); ?>
	                </div>
	              </div>
	            </div>
                <?php endwhile; ?>
            <?php else : ?>
              <div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>
            <?php endif; wp_reset_query();?>	
        </div>
    </div>
  </div>
</div><!--/sidebar -->