</div><!--/container -->
    </section>
    <footer class="footer">
        <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-5 col-lg-7">
			<div class="list-item">
				<div class="row">
					<?php dynamic_sidebar('footer');?>
				</div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
              <div class="list-item">
                <?php dynamic_sidebar('info-company');?>
              </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2">
          <div class="list-item">
            <h3>Mạng xã hội</h3>
             <div class="social"> 
                 <a href="https://www.facebook.com/dophuotzura"><span class="facebook"><i class="fa fa-facebook-official"></i> Facebook</span></a>
            </div> 
			<?php if( is_front_page() )  {
				$base_url = home_url();
			} else {
				$base_url = get_permalink();
			}; 
			?>
			<script src="https://apis.google.com/js/platform.js" async defer></script>
			<g:plusone a href="<?php echo $base_url; ?>"></g:plusone> 
            </div>
          </div>
        </div>
        <div class="row dev">
              <div class="col-xs-12 col-sm-6 col-md-9">
                <p>Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo("name"); ?>. All rights reserved.</p>                
                <p>Một sản phẩm được phát triển bởi <a href="http://zuravn.com/" title="" data-original-title="Thiết kế website đà nẵng">www.zuravn.com</a> </p>
              </div>
          </div>
      </div>
    </footer>
<a class="back-to-top"><i class="fa fa-angle-up"></i></a>
<div id="fb-root"></div>
<div class="support-icon-right">
            <h3><i class="fa fa-hand-o-right"></i> Chat Live Facebook</h3>
              <div class="online-support">  
          <div 
            class="fb-page" 
            data-href="https://www.facebook.com/dophuotzura" 
            data-small-header="true" 
            data-height="300"  
            data-width="250" 
            data-tabs="messages" 
            data-adapt-container-width="false" 
            data-hide-cover="false" 
            data-show-facepile="false" 
            data-show-posts="false">
          </div>
          </div>
          </div>
<?php wp_footer(); ?>
</body>
</html>