<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version 	2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
?>
<div class="detail-product clearfix row">
<?php if ( ! empty( $tabs ) ) : ?>
	<div class="col-xs-12 col-sm-8 col-md-9">
	<ul class="nav nav-tabs" role="tablist" id="tab-kenit">
       	<?php foreach ( $tabs as $key => $tab ) : ?>
			<?php if( isset($tab['title']) ) : ?>
			<li role="presentation" class="">
				<a href="#tab-<?php echo $key ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', $tab['title'], $key ) ?></a>
			</li>
			<?php endif; ?>
		<?php endforeach; ?>
    </ul>
    <div class="tab-content">
    	<?php foreach ( $tabs as $key => $tab ) : ?>
			<?php if( isset($tab['title']) ) : ?>
			<div role="tabpanel" class="tab-pane panel entry-content" id="tab-<?php echo $key ?>">
				<?php call_user_func( $tab['callback'], $key, $tab ) ?>
			</div>
			<?php endif; ?>
		<?php endforeach; ?>
    </div>
 	</div>
<?php endif; ?>
<div class="col-xs-12 col-sm-4 col-md-3 advisory">
	<h3 class="title"><span>Bài viết Tư vấn</span></h3>
	<?php  $args = array(
	      'post_type' => 'post',
	      'showposts' => '4',
	      'order' => 'DESC',
	      'orderby' => 'id',
	  );
	$news = new WP_Query($args); ?>
	<?php  if ($news->have_posts()) : ?>
	  <div class="clearfix list-post">
	      <?php while ($news->have_posts()) : $news->the_post();global $product;?>
	             <a class="post-img"  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
	                 <?php the_post_thumbnail('medium', array( 'class' => 'img-responsive center-block' ) ); ?>
	             </a>
	               <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-title">
	                  <?php echo kenit_limit_words(get_the_title(), 10); ?>
	               </a>
	      <?php endwhile; ?>
	      </div>
	  <?php else : ?>
	      <div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>
	  <?php endif; wp_reset_query();?>
</div>
</div>