<?php get_header()?>
        <div class="row content-top" id="banner">
          <div class="col-xs-12 col-sm-12 col-md-9 content col-md-push-3">
            <div id="owl-banner" class="owl-carousel owl-theme banner">
              <?php  $args = array(
                'post_type' => 'slides', 
                'showposts' => '100',
                'order' => 'DESC', 
                'orderby' => 'ID',                          
            ); 
            $news = new WP_Query($args); ?>
          <?php  if ($news->have_posts()) : ?>
                <?php while ($news->have_posts()) : $news->the_post();?>
                  <div class="item">
                    <?php the_post_thumbnail('full', array( 'class' => 'img-responsive center-block' ) ); ?>
                  </div>
                <?php endwhile; ?>
            <?php else : ?>
              <div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>
            <?php endif; wp_reset_query();?>
            </div>
			    
          </div><!--/content -->

        </div>
		
        <?php dynamic_sidebar('home');?>
		
        <ul class="nav nav-tabs" role="tablist" id="tab-kenit">
          <li role="presentation"><a href="#goc-tu-van" aria-controls="goc-tu-van" role="tab" data-toggle="tab">Tin Tức Mới</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="goc-tu-van">
                  <?php  $args = array(
                        'post_type' => 'post', 
                        'showposts' => '4',
                        'order' => 'DESC', 
                        'orderby' => 'id',                             
                    ); 
                  $news = new WP_Query($args); ?>
                  <?php  if ($news->have_posts()) : ?>
                    <div class="row clearfix advisory list-post">
                        <?php while ($news->have_posts()) : $news->the_post();global $product;?>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                          <div class="row">
                             <div class="col-xs-4 col-sm-4 col-md-4">
                               <a class="post-img"  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                   <?php the_post_thumbnail('medium', array( 'class' => 'img-responsive center-block' ) ); ?>
                               </a>
                             </div>
                             <div class="col-xs-8 col-sm-8 col-md-8">
                               <h2>
                                 <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-title">
                                     <?php the_title(); ?>
                                 </a>
                               </h2>
                               <p><?php echo kenit_limit_words(get_the_excerpt(), '15'); ?></p>  
                            </div>
                          </div>
                         </div>
                        <?php endwhile; ?>
                        </div>
                    <?php else : ?>
                        <div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>
                    <?php endif; wp_reset_query();?>
            </div>
        </div>
<?php get_footer() ?>