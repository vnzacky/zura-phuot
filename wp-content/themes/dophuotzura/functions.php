<?php 
/**
 * kenit functions and definitions
 *
 * @package kenit
 */
require_once('libs/logo-options/logo-options.php');
add_action( 'after_setup_theme', 'kenit_setup' );
if ( ! function_exists( 'kenit_setup' ) ):
    function kenit_setup() {
        register_nav_menu( 'menu-top', __('Menu Top') );
        register_nav_menu( 'menu-res', __('Menu Responsive') );
        register_nav_menu( 'menu-product', __('Menu Product') );
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         */
        add_theme_support( 'post-thumbnails' );
        add_image_size( 'product', 233, 284, true );
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		
	}
endif;
/**
 * Kenit site title
 * 
 */
function kenit_site_title() {
  global $page, $paged;
  wp_title( '|', true, 'right' );
  bloginfo( 'name' );
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Trang %s', 'kenit' ), max( $paged, $page ) );
}
/**
 * Register widget area.
 *
 */
function kenit_widgets_init() {
    register_sidebar( array(
    'name' => __( 'Home Pages', 'kenit' ),
    'id' => 'home',
    'description'   => __( 'Home', 'kenit' ),
    'before_widget' => '<div class="product-section">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="title"><span>',
    'after_title' => '</span></h3>',
  ) );
    register_sidebar( array(
    'name' => __( 'Giỏ hàng', 'kenit' ),
    'id' => 'cart',
    'description'   => __( 'Giỏ hàng', 'kenit' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ) );
    register_sidebar( array(
    'name' => __( 'Sidebar', 'kenit' ),
    'id' => 'sidebar',
    'description'   => __( 'Sidebar', 'kenit' ),
    'before_widget' => '<div class="block-sidebar">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="title"><span>',
    'after_title' => '</span></h3>',
  ) );
    register_sidebar( array(
    'name' => __( 'Footer', 'kenit' ),
    'id' => 'footer',
    'description'   => __( 'Footer', 'kenit' ),
    'before_widget' => '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  ) );
    register_sidebar( array(
    'name' => __( 'Info Company', 'kenit' ),
    'id' => 'info-company',
    'description'   => __( 'Info Company', 'kenit' ),
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
  ) );
}
add_action( 'widgets_init', 'kenit_widgets_init' );
/**
 *
 * 
 */
function kenit_limit_words($string, $word_limit)
{
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit) {
  array_pop($words);
  echo implode(' ', $words)."..."; } else {
  echo implode(' ', $words); }
}
/**
 * Enqueue scripts and styles.
 */
function kenit_scripts() {
  wp_enqueue_style('bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '' );
  wp_enqueue_style('zura-style', get_stylesheet_uri(), array(), time() );
  wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', array(), '' );
  wp_enqueue_style('font-awesome.min', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '' );
  wp_enqueue_style('owl.carousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '' );
  wp_enqueue_style('owl.theme.min', get_template_directory_uri() . '/css/owl.theme.css', array(), '' );
  wp_enqueue_style('editor-style', get_template_directory_uri() . '/css/editor-style.css', array(), '' );
  wp_enqueue_style('font-google', 'http://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto|Source+Sans+Pro&subset=latin,vietnamese,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek');
  wp_enqueue_script('jquery');
  wp_enqueue_script('bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0', true );
  wp_enqueue_script('wow.min', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.0', true );
  wp_enqueue_script('owl.carousel', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'), '1.0', true );
  wp_enqueue_script('zura-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'kenit_scripts' );
/**
 * 
 * Remove <p> Tags From Cat Description
 */
remove_filter('term_description','wpautop');
function kenit_plugin_facebook() { ?>
    <div class="like-click clearfix">
      <div class="fb-btn fb-send" data-href="<?php the_permalink(); ?>"></div>
      <div class="fb-btn fb-like" data-href="<?php the_permalink(); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
    </div>
    <div class="alert alert-success notice text-center" role="alert">Góp ý, phản hồi cho chúng tôi nhé</div>
    <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="15" data-colorscheme="light"></div>      
<?php } 
function slides() {
 
    $labels = array(
        'name' => _x('Banner','kenit'),
        'singular_name' => _x('Banner','kenit'),
        'add_new' => __ ('Thêm ảnh'),
        'add_new_item' => __('Thêm ảnh mới'),
        'edit_item' => __('Sửa ảnh'),
        'new_item' => __('Thêm ảnh mới'),
        'all_items' => __('Tất cả ảnh'),
        'view_item' => __('Xem ảnh'),
        'search_item' => __('Tìm ảnh'),
        'not_found' => __('Hiện tại chưa có ảnh nào'),
        'not_found_in_trash' => __('Không có ảnh nào trong sọt rác'),
        'menu_name' => 'Banner'
    );
    
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'rewrite' => true,
        'supports' => array('title', 'thumbnail'),
        'menu_position' => 5,
        'menu_icon' => 'dashicons-images-alt2',        
    );
    
    register_post_type( 'slides',$args);
    register_taxonomy( 'slides_category', 'slides', array( 'hierarchical' => true, 'label' => __('Hệ thống Banner'), 'rewrite' => array( 'slug' => __('slides') ) ) );
    flush_rewrite_rules();
}
add_action( 'init', 'slides' );
remove_filter( 'the_content', 'easy_image_gallery_append_to_content' ); 
function create_shortcode_product($args, $content) {
 
        $product_home = new WP_Query(array(
                'post_type' => 'product', 
                'showposts' => $args['showposts'],
                'order' => 'DESC', 
                'orderby' => 'random', 
                'tax_query' => array(
                                  array(
                                    'taxonomy' => 'product_cat',
                                    'field'    => 'term_id',
                                    'terms'    => $args['id'],
                                  ),
                                ),
        ));
 
        ob_start(); ?>
        <h3 class="title"><span>
          <?php
              $term_id = $args['id'];
              $taxonomy = 'product_cat';
              $catname = get_term($term_id, $taxonomy);
          ?>
          <?php echo $catname->name; ?>
        </span></h3>
            <?php if ( $product_home->have_posts() ) :?>
            <div class="list-content">   
              <div class="row"> 
              <?php while ( $product_home->have_posts() ) :$product_home->the_post(); global $product;?>
                  <?php
                      get_template_part( 'template-parts/content', 'pro' );
                  ?>
              <?php endwhile; ?>
              </div>
            </div>
            <?php endif; ?>
        
        <?php $list_product = ob_get_contents(); 
 
        ob_end_clean();
 
        return $list_product;
}
add_shortcode('kenit-products', 'create_shortcode_product');
add_filter('widget_text', 'do_shortcode');
/**
 * Replace WooCommerce Default Pagination with WP-PageNavi Pagination
 *
 * @author WPSnacks.com
 * @link http://www.wpsnacks.com
 */
 add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

remove_action('woocommerce_pagination', 'woocommerce_pagination', 10);
function woocommerce_pagination() {
        wp_pagenavi();      
    }
add_action( 'woocommerce_pagination', 'woocommerce_pagination', 10);
add_filter( 'woocommerce_currencies', 'add_my_currency' );
 
function add_my_currency( $currencies ) {
     $currencies['VND'] = __( 'Việt Nam Đồng', 'woocommerce' );
     return $currencies;
}
 
add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);
 
function add_my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'VND': $currency_symbol = 'VNĐ'; break;
     }
     return $currency_symbol;
} 
add_filter( 'woocommerce_breadcrumb_defaults', 'kenit_change_breadcrumb_delimiter' );
function kenit_change_breadcrumb_delimiter( $defaults ) {
    $defaults['delimiter'] = ' &raquo; ';
    return $defaults;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_city']);
     unset($fields['billing']['billing_country']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_postcode']);
     unset($fields['billing']['billing_state']);
     unset($fields['shipping']['shipping_state']);
     unset($fields['shipping']['shipping_city']);
     unset($fields['shipping']['shipping_country']);
     unset($fields['shipping']['shipping_address_2']);
     unset($fields['shipping']['shipping_postcode']);
     return $fields;
}  
/**
* is_realy_woocommerce_page - Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
*
* @access public
* @return bool
*/
function rtc_is_woocommerce_page () {
        if(  function_exists ( "is_woocommerce" ) && is_woocommerce()){
                return true;
        }
        $woocommerce_keys   =   array ( "woocommerce_shop_page_id" ,
                                        "woocommerce_terms_page_id" ,
                                        "woocommerce_cart_page_id" ,
                                        "woocommerce_checkout_page_id" ,
                                        "woocommerce_pay_page_id" ,
                                        "woocommerce_thanks_page_id" ,
                                        "woocommerce_myaccount_page_id" ,
                                        "woocommerce_edit_address_page_id" ,
                                        "woocommerce_view_order_page_id" ,
                                        "woocommerce_change_password_page_id" ,
                                        "woocommerce_logout_page_id" ,
                                        "woocommerce_lost_password_page_id" ) ;
        foreach ( $woocommerce_keys as $wc_page_id ) {
                if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
                        return true ;
                }
        }
        return false;
}
add_filter( 'woocommerce_enqueue_styles', 'ken_dequeue_styles' );
function ken_dequeue_styles( $enqueue_styles ) {
  unset( $enqueue_styles['woocommerce-general'] ); 
  unset( $enqueue_styles['woocommerce-layout'] );  
  unset( $enqueue_styles['woocommerce-smallscreen'] ); 
  return $enqueue_styles;
}
add_filter( 'woocommerce_enqueue_styles', '__return_false' );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 *
 */ 
function woo_related_products_limit() {
  global $product;
    
    $args['posts_per_page'] = 5;
    return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'kenit_related_products_args' );
  function kenit_related_products_args( $args ) {
 
    $args['posts_per_page'] = 5; 
    $args['columns'] = 2;
    return $args;
}
function remove_footer_admin () {
    echo 'Phát triển bởi www.ZuraVN.com';
}
add_filter('admin_footer_text', 'remove_footer_admin');
add_filter('show_admin_bar', '__return_false');

add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

  $tabs['reviews']['priority'] = 15;     // Reviews first
  $tabs['description']['priority'] = 5;      // Description second

  return $tabs;
}
/**
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
  
  
  $tabs['test_tab'] = array(
    'title'   => __( 'Thông số kỹ thuật', 'woocommerce' ),
    'priority'  => 10,
    'callback'  => 'woo_new_product_tab_content'
  );

  return $tabs;

}
function woo_new_product_tab_content() {
  echo types_render_field("thong-so-ky-thuat", array( ));
}
**/
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;

  ob_start();

  ?>
  <span class="cart-count">[<?php echo $woocommerce->cart->cart_contents_count; ?> Items]</span>
  <?php

  $fragments['span.cart-count'] = ob_get_clean();

  return $fragments;

}


function kenit_easy_image_gallery() {
    $attachment_ids = easy_image_gallery_get_image_ids();

  global $post;

  if ( $attachment_ids ) { ?>

    <?php

    $has_gallery_images = get_post_meta( get_the_ID(), '_easy_image_gallery', true );

    if ( !$has_gallery_images )
      return;


    $has_gallery_images = explode( ',', get_post_meta( get_the_ID(), '_easy_image_gallery', true ) );


    $has_gallery_images = array_filter( $has_gallery_images );

    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'feature' );
    $image_title = esc_attr( get_the_title( get_post_thumbnail_id( $post->ID ) ) );


    $classes = array();


    $classes[] = $has_gallery_images ? 'thumbnails-' . easy_image_gallery_count_images() : '';


    $classes[] = easy_image_gallery_has_linked_images() ? 'linked' : '';

    $classes = implode( ' ', $classes );

    ob_start();
?>
<div id="gallery-full" class="owl-carousel">
    <?php
    foreach ( $attachment_ids as $attachment_id ) {

      $classes = array( 'popup' );

      $image_link = wp_get_attachment_image_src( $attachment_id, apply_filters( 'easy_image_gallery_linked_image_size', 'large' ) );
      $image_link = $image_link[0]; 

      $image = wp_get_attachment_image( $attachment_id, apply_filters( 'easy_image_gallery_thumbnail_image_size', 'large' ), '', array( 'class' => 'img-responsive center-block', 'alt' => trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) ) ) );

      $image_caption = get_post( $attachment_id )->post_excerpt ? esc_attr( get_post( $attachment_id )->post_excerpt ) : '';

      $image_class = esc_attr( implode( ' ', $classes ) );

      $lightbox = easy_image_gallery_get_lightbox();

      $rel = easy_image_gallery_count_images() > 1 ? 'rel="'. $lightbox .'[group]"' : 'rel="'. $lightbox .'"';

      if ( easy_image_gallery_has_linked_images() )
        $html = sprintf( '<div class="item"><a %s href="%s" class="%s" title="%s">%s</a></div>', $rel, $image_link, $image_class, $image_caption, $image );
      else
        $html = sprintf( '<div class="item">%s</div>', $image );

      echo apply_filters( 'easy_image_gallery_html', $html, $rel, $image_link, $image_class, $image_caption, $image, $attachment_id, $post->ID );
    }
?>
</div>
<div id="gallery-thumbnail" class="owl-carousel">
    <?php
    foreach ( $attachment_ids as $attachment_id ) {

      $classes = array( 'popup' );

      $image_link = wp_get_attachment_image_src( $attachment_id, apply_filters( 'easy_image_gallery_linked_image_size', 'large' ) );
      $image_link = $image_link[0]; 

      $image = wp_get_attachment_image( $attachment_id, apply_filters( 'easy_image_gallery_thumbnail_image_size', 'thumbnail' ), '', array( 'class' => 'img-responsive center-block','alt' => trim( strip_tags( get_post_meta( $attachment_id, '_wp_attachment_image_alt', true ) ) ) ) );

      $image_caption = get_post( $attachment_id )->post_excerpt ? esc_attr( get_post( $attachment_id )->post_excerpt ) : '';

      $image_class = esc_attr( implode( ' ', $classes ) );

      $lightbox = easy_image_gallery_get_lightbox();

      $rel = easy_image_gallery_count_images() > 1 ? 'rel="'. $lightbox .'[group]"' : 'rel="'. $lightbox .'"';

      if ( easy_image_gallery_has_linked_images() )
        $html = sprintf( '<div class="item">%s</div>', $image );
      else
        $html = sprintf( '<div class="item">%s</div>', $image );

      echo apply_filters( 'easy_image_gallery_html', $html, $rel, $image_link, $image_class, $image_caption, $image, $attachment_id, $post->ID );
    }
?>
</div>
<?php 
  }
}
add_filter( 'easy_image_gallery', 'kenit_easy_image_gallery' );
?>