<?php get_header()?>
<?php the_post(); ?>
    <div class="row">
    <div class="content detail col-xs-12 col-sm-12 col-md-9">
        <h1 class="title-post"><span><?php the_title(); ?></span></h1>
         <div class="post-meta text-left"><span><i class="fa fa-calendar"></i> Đăng ngày:</span> <?php the_time("j M Y"); ?> <span> <i class="fa fa-user"></i> Đăng bởi:</span> <?php the_author_meta( 'first_name' ) ;?> <?php the_author_meta( 'last_name' ) ;?></div>
            <article class="post-content"><?php the_content(); ?></article>
            <p class="tags"> <?php the_tags( 'Tags: ', ', ', ',' ); ?></p> 
            <?php kenit_plugin_facebook(); ?>
             <?php
                $categories = get_the_category($post->ID);
                if ($categories) {
                    $category_ids = array();
                        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
                            $args=array(
                                'category__in' => $category_ids,
                                'post__not_in' => array($post->ID),
                                'showposts'=> 3,
                                'caller_get_posts'=> 1
                            );
                        $my_query = new wp_query($args);
                        if( $my_query->have_posts() ) {?>
                        <h3 class="title"><span>Tin tức khác</span></h3>
                        <?php while ($my_query->have_posts()) { $my_query->the_post(); ?>
                            <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                                get_template_part( 'template-parts/content', '' );
                            ?>
                            <?php }?>
                        <?php }
                    }  ?>
    </div>
    <?php get_sidebar();?>
</div>                               
<?php get_footer() ?>