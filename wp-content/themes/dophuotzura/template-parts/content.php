<?php
/**
 * @package kenit
 */
?>
<div class="row clearfix feature list-post">
 <div class="col-xs-12 col-sm-4 col-md-4">
   <a class="post-img"  href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
       <?php the_post_thumbnail('full', array( 'class' => 'img-responsive center-block' ) ); ?>
   </a>
 </div>
 <div class="col-xs-12 col-sm-8 col-md-8">
   <h4>
     <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-title">
         <?php the_title(); ?>
     </a>
   </h4>
   <p><?php the_excerpt();?></p>  
   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="btn-success btn">Chi tiết <i class="fa fa-angle-double-right"></i></a>
</div>
</div>