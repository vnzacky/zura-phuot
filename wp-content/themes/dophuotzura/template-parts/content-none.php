<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package kenit
 */
?>
<div class="alert alert-danger notice text-center" role="alert">Rất tiết, mục này chưa có dữ liệu.</div>