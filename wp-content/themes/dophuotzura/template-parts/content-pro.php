<?php
/**
 * @package kenit
 */
global $product;
?>
<div class="col-xs-6 col-sm-4 col-md-3 col-lg-5ths">
  <div class="product-item"> 
	  <?php 
		$featured = get_post_meta( get_the_ID(), '_featured', true );
		if ( $featured == 'yes') : ?>
			<span class="label left label-danger"><i class="fa fa-star-o"></i> Hot</span>
		<?php endif; ?>
		<?php if($product->is_on_sale() && $product->product_type == 'simple') : ?>
			<span class="label right label-info">
			<?php  
				$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
				echo $price . sprintf( __('%s', 'woocommerce' ), $percentage . '%' ); 
			?>
			</span>
		<?php endif; ?>
		<div class="details">
			<?php the_excerpt();?>
			<a href="<?php the_permalink(); ?>" class="btn-success btn" title="<?php the_title(); ?>"><i class="fa fa-desktop"></i> Chi tiết</a>
			<button type="submit" class="add_to_cart_button product_type_<?php echo $product->product_type; ?> btn-danger btn" data-product_id="<?php echo $product->id; ?>"> Mua hàng</button>
		</div>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php the_post_thumbnail('product', array( 'class' => 'img-responsive center-block' ) ); ?>
		</a>
		<div class="meta">
			<h2><a rel="tooltip" class="post-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
					<?php kenit_limit_words(get_the_title(), '10'); ?>
			</a></h2>
			<span class="price">
				<?php echo $product->get_price_html(); ?>
			</span>
		</div>
	</div>
</div> 