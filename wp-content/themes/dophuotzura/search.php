<?php get_header()?>
<?php 
global $wp_query;
$args = array_merge( $wp_query->query_vars, array( 'post_type' => 'product') );
query_posts( $args );
?>
        <h1 class="title-post"><span>Kết quả tìm kiếm</span></h1>
            <?php if(have_posts()) : ?>
                <div class="row">
                <?php while (have_posts()) : the_post(); ?>
                    <?php
                    /* Include the Post-Format-specific template for the content.
                     * If you want to override this in a child theme, then include a file
                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                     */
                    if( get_post_type() == 'product' ) {
                         get_template_part( 'template-parts/content', 'pro' );
                    } else  {
                         get_template_part( 'template-parts/content', '' );
                    }
                    ?>
                <?php endwhile; ?>
                </div>
                <?php if(function_exists('wp_pagenavi')): ?>
                    <?php wp_pagenavi(); ?>
                <?php endif; ?>
            <?php else : ?>
                <?php get_template_part( 'template-parts/content', 'none' ); ?>
            <?php endif; ?>      
<?php get_footer(); ?>