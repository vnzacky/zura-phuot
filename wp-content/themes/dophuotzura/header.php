<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index,follow"/>
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
  <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
  <![endif]--> 
  <!-- Bootstrap -->
  <link rel="profile" href="http://gmpg.org/xfn/11" />
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76820269-1', 'auto');
  ga('send', 'pageview');

</script>
<!--Menu Responsive-->
  <div class="menu-responsive">
    <div class="menu-close">
      <i class="fa fa-bars"></i>
      <span>Menu</span>
      <i class="fa fa-times"></i>
    </div>
    <?php wp_nav_menu(array(
      'theme_location'  => 'menu-product',
      'container'=> '0', 
      'container_class' => '0', 
      'container_id'=> '0', 
      'items_wrap' => '<ul>%3$s</ul>' )
      ); 
    ?>
  </div>
  <div class="menu-icon container">
    <div class="row">
      <div class="menu-open col-xs-2 col-sm-2 col-md-2">
        <i class="fa fa-bars"></i>
      </div>
      <div class=" col-xs-10 col-sm-10 col-md-10">
      <?php get_search_form();?>
      </div>
    </div>
  </div>
  <!--End Menu Responsive-->
<header id="header" class="">
      <div class="header-top">
      <div class="container"> 
        <div class="row">
         <div class="menu-top col-md-push-4 col-xs-12 col-sm-12 col-md-8">
            <?php wp_nav_menu(array(
              'theme_location'  => 'menu-top',
              'container'=> '0', 
              'container_class' => '0', 
              'container_id'=> '0', 
              'items_wrap' => '<ul>%3$s</ul>' )
              ); 
            ?>
          </div>
         </div>
      </div> 
    </div>
      <div class="container">
        <div class="row">
          <div class="logo col-xs-12 col-sm-12 col-md-3">
            <?php $logo_options = get_option('theme_logo_options'); ?>    
			<?php if (is_front_page()) : ?>
				<h1 id="logo">
					<a href="<?php echo esc_url(home_url('/')); ?>" rel="home" title="<?php bloginfo('name'); ?>">
						<span class="site-title"><?php bloginfo('name'); ?></span>
						<img src="<?php echo esc_url($logo_options['logo']); ?>" alt="<?php bloginfo('name'); ?>">
					</a>
				</h1> 
			<?php else : ?>
				<p id="logo">
					<a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
						<span class="site-title"><?php bloginfo('name'); ?></span>
						<img src="<?php echo esc_url($logo_options['logo']); ?>" alt="<?php bloginfo('name'); ?>">
					</a>
				</p>
			<?php endif; ?>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-5 search">
                <?php get_search_form();?>
                <div class="tags"><strong>Từ khóa:</strong> 
                  <?php 
                      $args = array(
                        'show_option_all'    => '',
                        'orderby'            => 'name',
                        'order'              => 'ASC',
                        'style'              => 'list',
                        'hide_empty'         => 0,
                        'use_desc_for_title' => 1,
                        'child_of'           => 0,
                        'hierarchical'       => 1,
                        'show_count'         => 1,
                        'title_li'           => '',
                        'taxonomy'           => 'product_tag',
                      );
                    ?>
                    <ul>
                    <?php wp_list_categories( $args ); ?>
                    </ul>
                </div>
          </div>
          <div class="tools col-xs-12 col-sm-12 col-md-4">
            <?php
                    if ( is_user_logged_in() ) {
                        $current_user = wp_get_current_user();
                    ?>
                    <a href="<?php echo get_permalink( get_page_by_path('my-account')); ?>" class="btn btn-warning"><i class="fa fa-sign-in"></i> 
                    <?php 
                      if ($current_user->user_firstname!='') {
                        echo $current_user->user_firstname;
                        echo $current_user->user_lastname;
                      } else {
                        echo 'Ẩn danh';
                      }
                    ?>     
                    </a>
                    <?php } else { ?>
                    <a href="<?php echo get_permalink( get_page_by_path('my-account')); ?>" class="btn btn-success"><i class="fa fa-sign-in"></i> Đăng nhập</a>
                    <?php } ?>
                <a data-toggle="modal" data-target="#mysupport" role="button" class="btn btn-info"><i class="fa fa-sign-out"></i> Giỏ hàng</a>
                <a href="<?php echo get_permalink( get_page_by_path('checkout')); ?>" class="button btn-check-order btn btn-primary">
                  <i class="fa fa-truck"></i> 
                  <span>Đơn hàng</span>
                </a>
                <!-- Modal -->
                <div class="modal fade" id="mysupport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Thông tin giỏ hàng</h4>
                        <span></span>
                      </div>
                      <div class="modal-body">
                        <div class="description">
                          <?php dynamic_sidebar('cart');?>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <p class="text-center"><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
                <!--End Modal --><!-- .modal -->  
              </div>
        </div>
      </div>
    </header><!-- /header -->
    <nav class="nav">
      <div class="container">
        <div class="row">
        <div class="col-md-3">
          <div class="category visible-md visible-lg">
            <h3 class="title"><span><i class="fa fa-list"></i> Danh mục Sản phẩm</span></h3>
              <nav id="menu" class="menu-cat <?php if(!is_home()):?>menu-category<?php endif;?>">
                <?php 
                  $args = array(
                    'show_option_all'    => '',
                    'orderby'            => 'name',
                    'order'              => 'ASC',
                    'style'              => 'list',
                    'show_count'         => 0,
                    'hide_empty'         => 0,
                    'use_desc_for_title' => 1,
                    'child_of'           => 0,
                    'hierarchical'       => 1,
                    'title_li'           => '',
                    'taxonomy'           => 'product_cat',
                  );
                ?>
                <ul>
                <?php wp_list_categories( $args ); ?>
                </ul>
              </nav>
            </div>
          </div>
          <div class="col-md-5">
             <a class="home" href="<?php bloginfo("home"); ?>" title="<?php bloginfo("name"); ?>">
                <i class="fa fa-home"></i>  
             </a>
            <?php get_search_form();?>
          </div>
          <div class="hotline col-md-2">
              <p style="margin-top: 15px;"><span>Đà Nẵng:</span> 0936 066 952</p>
          </div>
          <div class="cart col-md-2">
            <a href="<?php echo get_permalink( get_page_by_path('cart')); ?>" class="btn btn-info">
              <i class="fa fa-shopping-cart"></i> 
              Giỏ hàng 
              <span class="cart-count"></span>
            </a>
          </div>
        </div>
      </div>
    </nav>
    <section>
      <div class="container height">