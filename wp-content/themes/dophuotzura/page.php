<?php get_header()?>
<?php the_post(); ?>
 <?php if ( !rtc_is_woocommerce_page()) { ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-9 content">
            <h1 class="title-page"><span><?php the_title(); ?></span></h1>
            <div class="post-meta text-left"><span><i class="fa fa-calendar"></i> Đăng ngày:</span> <?php the_time("j M Y"); ?> <span> <i class="fa fa-user"></i> Đăng bởi:</span> <?php the_author_meta( 'first_name' ) ;?> <?php the_author_meta( 'last_name' ) ;?></div>
            <article class="post-content"><?php the_content(); ?></article> 
            <?php kenit_plugin_facebook();?>
        </div>
        <?php get_sidebar();?>
    </div>                       
<?php } else { ?>
    <h1 class="title-page"><span><?php the_title(); ?></span></h1>
    <article class="post-content"><?php the_content(); ?></article> 
<?php } ?>
<?php get_footer() ?>