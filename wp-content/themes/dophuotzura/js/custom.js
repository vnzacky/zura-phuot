(function($){
    $("iframe").wrap('<div class="embed-responsive embed-responsive-16by9"/>');
    $("iframe").addClass('embed-responsive-item');

    $(document).ready(function () {
        $("#owl-banner").owlCarousel({
            autoPlay: 3000,
            navigation: false,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            pagination: false
        });
        for (var i = 0; i < 10; i++) {
            $("#owl-product-" + i).owlCarousel({
                autoPlay: 3000,
                navigation: false,
                slideSpeed: 300,
                paginationSpeed: 400,
                items: 5,
                pagination: false
            });
        }
        $("#owl-new").owlCarousel({
            autoPlay: 3000,
            navigation: false,
            slideSpeed: 500,
            paginationSpeed: 500,
            singleItem: true,
            pagination: false
        });
    });

    $(document).ready(function () {

        var sync1 = $("#gallery-full");
        var sync2 = $("#gallery-thumbnail");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            autoPlay: true,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
        });

        sync2.owlCarousel({
            items: 5,
            itemsDesktop: [1199, 10],
            itemsDesktopSmall: [979, 10],
            itemsTablet: [768, 8],
            itemsMobile: [479, 4],
            autoPlay: true,
            pagination: false,
            responsiveRefreshRate: 100,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        function syncPosition(el) {
            var current = this.currentItem;
            $("#gallery-thumbnail")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if ($("#gallery-thumbnail").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#gallery-thumbnail").on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }
		
		$('.woocommerce .thumbnails').owlCarousel({
            slideSpeed: 1000,
            items: 4,
            pagination: false,
            autoPlay: true,
        });

    });
    jQuery(document).ready(function () {
        var offset = 220;
        var duration = 500;
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('.back-to-top').fadeIn(duration);
            } else {
                jQuery('.back-to-top').fadeOut(duration);
            }
        });
        jQuery('.back-to-top').click(function (event) {
            event.preventDefault();
            jQuery('html, body').animate({
                scrollTop: 0
            }, duration);
            return false;
        })
    });

    new WOW().init();

    function menu() {
        $('.menu-open').click(function () {
            $('.menu-responsive').animate({left: '0px'}, 200);
        });
        $('.menu-close').click(function () {
            $('.menu-responsive').animate({left: '-250px'}, 200);
        });
    };
    $(document).ready(menu);
    $('[rel="tooltip"]').tooltip();
    $(function () {
        $('#menu li').on('mouseover', function (e) {
            if ($(this).has('ul').length) {
                $(this).parent().addClass('expanded');
            }
            $('ul:first', this).parent().find('> a').addClass('active');
            $('ul:first', this).show();
        }).on('mouseout', function (e) {
            $(this).parent().removeClass('expanded');
            $('ul:first', this).parent().find('> a').removeClass('active');
            $('ul:first', this).hide();
        });
    });
    $(document).ready(function () {
        $('.cart-widget').hide();
        $('a.view-cart').click(function (e) {
            e.stopPropagation();
            $('.cart-widget').slideToggle();
        });
        $('.cart-widget').click(function (e) {
            e.stopPropagation();
        });
        $(document).click(function () {
            $('.cart-widget').slideUp();
        });
    });

    $(".nav-tabs li").first().addClass("active");
    $(".tab-pane").first().addClass("active");


    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 450) {
            $("nav.nav").addClass("navfixed");
        } else {
            $("nav.nav").removeClass("navfixed");
        }
    });

    $("div.category").hover(
        function () {
            $(this).addClass('hover');
        },
        function () {
            $(this).removeClass("hover");
        }
    );
    $(document).ready(function () {
        $('.online-support').hide();
        $('.support-icon-right h3').click(function (e) {
            e.stopPropagation();
            $('.online-support').slideToggle();
        });
        $('.online-support').click(function (e) {
            e.stopPropagation();
        });
        $(document).click(function () {
            $('.online-support').slideUp();
        });
    });
}(jQuery));
