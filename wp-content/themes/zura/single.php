<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package Zura
 * @subpackage ZuraCompany
 * @since ZuraVN.Com 1.0
 */

get_header(); ?>

<?php zura_page_element(); ?>

<div class="<?php zura_main_class(); ?>">
	<div class="row">
		<section id="primary" class="col-md-8">
			<div class="inner">
				<?php
					do_action('zura_before_single_post_loop');
				?>
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

					// Include the single post content template.
					get_template_part( 'templates/single/content', get_post_format() );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						?>
						<h3 class="section-title">Gửi bình luận</h3>
						<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>
						<?php					
					}
					// End of the loop.
				endwhile;
				?>
				
				<div class="navigation row">
					<div class="container-fluid">
						<h3 class="section-title">Bài viết tiếp theo</h3>
						<div class="col-md-6">
						<?php previous_post_link(); ?>
						</div>
						<div class="col-md-6">
						<?php next_post_link(); ?>
						</div>
					</div>
				</div> <!-- end navigation -->
				<?php
					do_action('zura_after_single_post_loop');
				?>
			</div>
		</section><!-- #primary -->
		<section id="sidebar" class="col-md-4">
			<?php get_sidebar(); ?>
		</section><!-- #sidebar -->	
	</div><!--.row-->
</div><!--.content-wrap-->

<?php get_footer(); ?>
