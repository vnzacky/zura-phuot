<?php

$this->addSection(array(
    'title' => __('General', 'zura'),
    'id' => 'general',
    'icon' => 'el el-home',
    'fields' => array()
));
$this->addSection(array(
    'title' => __('Homepage', 'zura'),
    'id' => 'info',
    'icon' => 'el el-th',
    'fields' => array(
        array(
            'title' => esc_html__('Enable Banner', 'zura'),
            'subtitle' => esc_html__('Show/Hide Banner.', 'zura'),
            'id' => 'banner_enable',
            'type' => 'switch',
            'default' => false,
        ),
        array(
            'title' => esc_html__('Enable News Section', 'zura'),
            'subtitle' => esc_html__('Show/Hide News Section.', 'zura'),
            'id' => 'news_enable',
            'type' => 'switch',
            'default' => false,
        ),
    )
));
$this->addSection(array(
    'title' => __('News', 'zura'),
    'id' => 'news_section',
    'icon' => 'el el-th',
    'subsection' => true,
    'fields' => array(
        array(
            'title' => esc_html__('Post Per Page', 'zura'),
            'subtitle' => esc_html__('Number posts for display.', 'zura'),
            'id' => 'news_post_per_page',
            'type' => 'text',
            'default' => __(6, 'zura')
        ),
        array(
            'title' => esc_html__('Label 1', 'zura'),
            'subtitle' => esc_html__('Display title for tab news 1.', 'zura'),
            'id' => 'news_tab_label_1',
            'type' => 'text',
            'default' => __('News Tab 1', 'zura')
        ),
        array(
            'id'       => 'news_tab_cat_1',
            'type'     => 'select',
            'multi'    => true,
            'title'    => __('Category 1', 'zura'),
            'subtitle' => __('Display list news for get from list categories. *Leave empty for get all categories', 'zura'),
            'data'     => 'categories'
        ),
        array(
            'title' => esc_html__('Label 2', 'zura'),
            'subtitle' => esc_html__('Display title for tab news 2.', 'zura'),
            'id' => 'news_tab_label_2',
            'type' => 'text',
            'default' => __('News Tab 2', 'zura')
        ),
        array(
            'id'       => 'news_tab_cat_2',
            'type'     => 'select',
            'multi'    => true,
            'title'    => __('Category 2', 'zura'),
            'subtitle' => __('Display list news for get from list categories. *Leave empty for get all categories', 'zura'),
            'data'     => 'categories'
        ),

        array(
            'title' => esc_html__('Label 3', 'zura'),
            'subtitle' => esc_html__('Display title for tab news 3.', 'zura'),
            'id' => 'news_tab_label_3',
            'type' => 'text',
            'default' => __('News Tab 3', 'zura')
        ),
        array(
            'title'    => __('Category 3', 'zura'),
            'subtitle' => __('Display list news for get from list categories. *Leave empty for get all categories', 'zura'),
            'id'       => 'news_tab_cat_3',
            'type'     => 'select',
            'multi'    => true,
            'data'     => 'categories'
        ),

    )
));
/* Logo */
$this->addSection(array(
    'title' => esc_html__('Header', 'zura'),
    'icon' => 'el-icon-picture',
    'fields' => array(
        array(
            'id'       => 'header_height',
            'type'     => 'dimensions',
            'units'    => array('px'),
            'title'    => __('Header Height', 'zura'),
            'width'    => false,
            'default'  => array(
                'height'  => 80
            ),
        ),
        array(
            'title' => esc_html__('Select Logo', 'zura'),
            'subtitle' => esc_html__('Select an image file for your logo.', 'zura'),
            'id' => 'main_logo',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url' => get_template_directory_uri() . '/logo.png'
            )
        ),
        array(
            'subtitle' => esc_html__('Change Sticky Logo.', 'zura'),
            'id' => 'sticky_logo_enable',
            'type' => 'switch',
            'title' => esc_html__('Enable Sticky Logo ', 'zura'),
            'default' => false,
        ),
        array(
            'title' => esc_html__('Select Logo', 'zura'),
            'subtitle' => esc_html__('Select an image file for your logo.', 'zura'),
            'id' => 'sticky_logo',
            'type' => 'media',
            'url' => true,
            'default' => array(
                'url' => get_template_directory_uri() . '/logo.png'
            ),
            'required' => array('sticky_logo_enable', '=', 1)
        ),
        array(
            'id'       => 'header_sticky_height',
            'type'     => 'dimensions',
            'units'    => array('px'),
            'title'    => __('Header Sticky Height', 'zura'),
            'width'    => false,
            'default'  => array(
                'height'  => 80
            ),
            'required' => array('sticky_logo_enable', '=', 1)
        ),
    )
));

/**
 * Settings Color.
 */
$this->addSection(array(
    'title' => esc_html__('Navigation', 'zura'),
    'icon' => 'el-icon-adjust',
    'fields' => array(
        array(
            'id' => 'menu_link_color',
            'subtitle' => esc_html__('set menu link color.', 'zura'),
            'type' => 'color',
            'title' => esc_html__('Link Color', 'zura'),
            'default' => '#D86838'
        ),
        array(
            'id' => 'menu_link_hover_color',
            'subtitle' => esc_html__('set menu link hover color.', 'zura'),
            'type' => 'color',
            'title' => esc_html__('Link Hover Color', 'zura'),
            'default' => '#D86838'
        ),
        array(
            'id' => 'menu_link_background_color',
            'subtitle' => esc_html__('set menu link background color.', 'zura'),
            'type' => 'color',
            'title' => esc_html__('Link Background Color', 'zura'),
            'default' => 'transparent'
        ),
        array(
            'id' => 'menu_link_background_hover_color',
            'subtitle' => esc_html__('set menu link background hover color.', 'zura'),
            'type' => 'color',
            'title' => esc_html__('Link Background Hover Color', 'zura'),
            'default' => 'transparent'
        ),

    )
));

/**
 * Settings Color.
 */
$this->addSection(array(
    'title' => esc_html__('Styling', 'zura'),
    'icon' => 'el-icon-adjust',
    'fields' => array(
        array(
            'subtitle' => esc_html__('set color main color.', 'zura'),
            'id' => 'primary_color',
            'type' => 'color',
            'title' => esc_html__('Primary Color', 'zura'),
            'default' => '#D86838'
        ),
        array(
            'id' => 'secondary_color',
            'type' => 'color',
            'title' => esc_html__('Secondary Color', 'zura'),
            'default' => '#D86838'
        ),
        array(
            'subtitle' => esc_html__('set color for tags <a></a>.', 'zura'),
            'id' => 'link_color',
            'type' => 'link_color',
            'title' => esc_html__('Link Color', 'zura'),
            'output' => array('a'),
            'default' => array(
                'regular' => '#3c3c3c',
                'hover' => '#D86838',
            )
        )
    )
));


/**
 * Typography
 *
 * @author ZuraVN
 */
$this->addSection(array(
    'title' => esc_html__('Typography', 'zura'),
    'icon' => 'el-icon-text-width',
    'fields' => array(
        array(
            'id' => 'font_body',
            'type' => 'typography',
            'title' => esc_html__('Body Font', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body'),
            'units' => 'px',
            'default' => array(
                'color' => '#676767',
                'font-style' => 'normal',
                'font-weight' => '400',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '14px',
                'line-height' => '30px',
                'text-align' => ''
            ),
            'subtitle' => esc_html__('Typography option with each property can be called individually.', 'zura'),
        ),
        array(
            'id' => 'font_h1',
            'type' => 'typography',
            'title' => esc_html__('H1', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body h1'),
            'units' => 'px',
            'default' => array(
                'color' => '#3c3c3c',
                'font-style' => 'normal',
                'font-weight' => '700',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '36px',
                'line-height' => '40px',
                'text-align' => ''
            )
        ),
        array(
            'id' => 'font_h2',
            'type' => 'typography',
            'title' => esc_html__('H2', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body h2'),
            'units' => 'px',
            'default' => array(
                'color' => '#3c3c3c',
                'font-style' => 'normal',
                'font-weight' => '700',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '30px',
                'line-height' => '36px',
                'text-align' => ''
            )
        ),
        array(
            'id' => 'font_h3',
            'type' => 'typography',
            'title' => esc_html__('H3', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body h3'),
            'units' => 'px',
            'default' => array(
                'color' => '#3c3c3c',
                'font-style' => 'normal',
                'font-weight' => '700',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '24px',
                'line-height' => '28px',
                'text-align' => ''
            )
        ),
        array(
            'id' => 'font_h4',
            'type' => 'typography',
            'title' => esc_html__('H4', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body h4'),
            'units' => 'px',
            'default' => array(
                'color' => '#3c3c3c',
                'font-style' => 'normal',
                'font-weight' => '700',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '20px',
                'line-height' => '24px',
                'text-align' => ''
            )
        ),
        array(
            'id' => 'font_h5',
            'type' => 'typography',
            'title' => esc_html__('H5', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body h5'),
            'units' => 'px',
            'default' => array(
                'color' => '#3c3c3c',
                'font-style' => 'normal',
                'font-weight' => '700',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '18px',
                'line-height' => '24px',
                'text-align' => ''
            )
        ),
        array(
            'id' => 'font_h6',
            'type' => 'typography',
            'title' => esc_html__('H6', 'zura'),
            'google' => true,
            'font-backup' => true,
            'all_styles' => true,
            'output' => array('body h6'),
            'units' => 'px',
            'default' => array(
                'color' => '#3c3c3c',
                'font-style' => 'normal',
                'font-weight' => '700',
                'font-family' => 'Roboto Condensed',
                'google' => true,
                'font-size' => '14px',
                'line-height' => '24px',
                'text-align' => ''
            )
        )
    )
));

/**
 * Body
 *
 * @author ZoTheme
 */
$this->addSection(array(
    'title' => esc_html__('Body', 'zura'),
    'icon' => 'el-icon-website',
    'fields' => array(
        array(
            'id' => 'body_background',
            'type' => 'background',
            'title' => esc_html__('Background', 'zura'),
            'subtitle' => esc_html__('body background with image, color, etc.', 'zura'),
            'output' => array('body'),
        ),
        array(
            'id' => 'body_margin',
            'title' => esc_html__('Margin', 'zura'),
            'type' => 'spacing',
            'units' => 'px',
            'mode' => 'margin',
            'output' => array('body #page'),
            'default' => array(
                'margin-top' => '0',
                'margin-right' => '0',
                'margin-bottom' => '0',
                'margin-left' => '0',
                'units' => 'px',
            )
        ),
        array(
            'id' => 'body_padding',
            'title' => esc_html__('Padding', 'zura'),
            'type' => 'spacing',
            'units' => 'px',
            'mode' => 'padding',
            'output' => array('body #page'),
            'default' => array(
                'padding-top' => '0',
                'padding-right' => '0',
                'padding-bottom' => '0',
                'padding-left' => '0',
                'units' => 'px',
            )
        ),
    )
));
/**
 * Optimal Core
 *
 * Optimal options for theme. optimal speed
 * @author ZuraVN
 */
$this->addSection(array(
    'title' => esc_html__('Optimal Core', 'zura'),
    'icon' => 'el-icon-idea',
    'fields' => array(
        array(
            'subtitle' => esc_html__('no minimize , generate css overtime...', 'zura'),
            'id' => 'dev_mode',
            'type' => 'switch',
            'title' => esc_html__('Dev Mode (not recommended)', 'zura'),
            'default' => false
        )
    )
));