<?php

class Zura_New_Products_Widget extends WP_Widget
{

    var $plugin_slug;  // identifier of this plugin for WP
    var $plugin_version; // number of current plugin version

    function __construct()
    {
        $this->plugin_slug = 'zura-new-products-widget';
        parent::__construct(
            $this->plugin_slug, // Base ID
            esc_html__('ZURA New Products', 'zura'), // Name
            array('description' => esc_html__('Show list new products', 'zura'),) // Args
        );
        $this->plugin_version = '1.0.0';
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'];
            if( !empty($instance['title_url']) ) {
                printf('<a href="%s" title="%s">%s</a>', $instance['title_url'], $instance['title'], apply_filters('widget_title', $instance['title']));
            } else {
                echo apply_filters('widget_title', $instance['title']);
            }
            echo $args['after_title'];
        }
        echo do_shortcode('[recent_products 
            per_page="'.$instance['number_posts'].'" 
            orderby="date" 
            order="desc" 
            columns="'.$instance['number_columns'].'" 
            category="'.$instance['category'].'"]'
        );
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     * @return string|void
     */
    public function form($instance)
    {
        $title = isset($instance['title']) ? $instance['title'] : __('New title', 'text_domain');
        $number_posts = isset($instance['number_posts']) ? $instance['number_posts'] : 4;
        $number_columns = isset($instance['number_columns']) ? $instance['number_columns'] : 4;
        $title_url = isset($instance['title_url']) ? $instance['title_url'] : '';
        $category = isset($instance['category']) ? $instance['category'] : '';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('title_url'); ?>"><?php _e('Link to title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title_url'); ?>"
                   name="<?php echo $this->get_field_name('title_url'); ?>" type="text"
                   value="<?php echo esc_attr($title_url); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Categories:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>"
                   name="<?php echo $this->get_field_name('category'); ?>" type="text"
                   value="<?php echo esc_attr($category); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number_posts'); ?>"><?php _e('Number posts:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_posts'); ?>"
                   name="<?php echo $this->get_field_name('number_posts'); ?>" type="number"
                   value="<?php echo esc_attr($number_posts); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number_columns'); ?>"><?php _e('Number columns:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_columns'); ?>"
                   name="<?php echo $this->get_field_name('number_columns'); ?>" type="number"
                   value="<?php echo esc_attr($number_columns); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['number_posts'] = (!empty($new_instance['number_posts'])) ? strip_tags($new_instance['number_posts']) : '';
        $instance['number_columns'] = (!empty($new_instance['number_columns'])) ? strip_tags($new_instance['number_columns']) : '';
        $instance['title_url'] = (!empty($new_instance['title_url'])) ? strip_tags($new_instance['title_url']) : '';
        $instance['category'] = (!empty($new_instance['category'])) ? strip_tags($new_instance['category']) : '';
        return $instance;
    }
}

/**
 * Register widget on init
 *
 * @since 1.0
 */

add_action('widgets_init', function () {
    if( class_exists('Woocommerce')) {
        register_widget('Zura_New_Products_Widget');
    }
});