<?php

/**
 * Auto create css from Meta Options.
 *
 * @author Zura
 * @version 1.0.0
 */
class Zura_DynamicCss
{

    function __construct()
    {
        add_action('wp_head', array($this, 'generate_css'));
    }

    /**
     * generate css inline.
     *
     * @since 1.0.0
     */
    public function generate_css()
    {
        global $smof_data;
        $css = $this->css_render();
    }

    /**
     * header css
     *
     * @since 1.0.0
     * @return string
     */
    public function css_render()
    {
        global $smof_data, $zo_meta;
        ob_start();

        return ob_get_clean();
    }
}

new Zura_DynamicCss();