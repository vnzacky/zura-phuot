<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Zura
 * @subpackage ZuraCompany
 * @since ZuraVN.Com 1.0
 */
?>
</div><!-- End #main-content -->
</div><!--.site-inner-->
<footer id="footer">
	<div id="footer-top">
		<div class="container">
			<div class="row">
				<?php if ( is_active_sidebar( 'footer-top-1' )  ) : ?>
					<div class="footer-top-1 col-md-3 col-sm-6">
						<?php dynamic_sidebar( 'footer-top-1' ); ?>
					</div>
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'footer-top-2' )  ) : ?>
					<div class="footer-top-2 col-md-3 col-sm-6">
						<?php dynamic_sidebar( 'footer-top-2' ); ?>
					</div>
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'footer-top-3' )  ) : ?>
					<div class="footer-top-3 col-md-3 col-sm-6">
						<?php dynamic_sidebar( 'footer-top-3' ); ?>
					</div>
				<?php endif; ?>
				<?php if ( is_active_sidebar( 'footer-top-4' )  ) : ?>
					<div class="footer-top-4 col-md-3 col-sm-6">
						<?php dynamic_sidebar( 'footer-top-4' ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div><!-- Footer Top -->
	<div id="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="copyright col-sm-6">
					<?php if ( is_active_sidebar( 'copyright' )  ) : ?>
						<?php dynamic_sidebar( 'copyright' ); ?>
					<?php endif; ?>
				</div>
				<div class="power col-sm-6">
					Thuộc hệ thống của <a href="http://zuravn.com">www.ZuraVN.com</a>
				</div>
			</div>
		</div>
	</div>
</footer><!-- #footer-->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
