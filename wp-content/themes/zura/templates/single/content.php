<?php
/**
 * The template part for displaying single posts
 *
 * @package Zura
 * @subpackage ZuraCompany
 * @since ZuraVN.Com 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		do_action('zura_before_single_post_summary');
	?>
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'zura' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'zura' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'templates/content/biography' );
			}
		?>
	</div><!-- .entry-content -->
	<?php
		do_action('zura_after_single_post_summary');
	?>
</article><!-- #post-## -->
