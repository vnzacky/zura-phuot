<?php
/**
 * The template part for displaying content
 *
 * @package Zura
 * @subpackage ZuraCompany
 * @since ZuraVN.Com 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-item'); ?> style="padding-bottom: 15px;">
	<div class="row">
		<div class="col-sm-3 post-thumbnail">
			<?php the_post_thumbnail('news_thumbnail'); ?>
		</div>
		<div class="col-sm-9 post-info">
			<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div><!-- .entry-content -->
		</div>
	</div>
</article><!-- #post-## -->
