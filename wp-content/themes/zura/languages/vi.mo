��    ^           �      �  *   �  &   $  
   K     V     Z     o     �     �     �  
   �     �     �     �     �     �     �     �     	      	     (	     1	     >	     F	     V	     ^	  1   j	     �	     �	     �	     �	  E   �	  W   

     b
     h
  =   |
     �
     �
  	   �
     �
     �
     �
                 L   .  %   {     �     �     �     �     �     �     �     �  "        '     4  F   F     �     �     �     �     �     �     �  \   �     V     ]  7   w     �     �  &   �  $   �       #   (     L     h     q  	   �     �     �     �     �     �  
   �  	   �     �     �  $     8   )      b     �     �  �  �     e     v     �  	   �  #   �  %   �     �     �               *     6  .   B     q     v     �     �     �     �     �     �  
   �     �  	          3        I     O     U      Z  W   {  �   �     U     b  M   �     �     �  
   �     �       *        G     V     v  e   �     �               #     0     <     U     e     x     �     �     �  |   �     <     T     o     u     �  ;   �     �  �   �     �  )   �  @   �     �     �  
     
             +     <     A     N     n  (   z     �     �     �     �     �     �          #  3   ;  '   o     �     �     �               A   7   Z       D   X      :   R   ,   N   3         F   +   .   6   J   H                                4       )                       [      Q   '           8   O   @              S       K   L   G   0             E   I   B       M       =      #   C   -          V   &             ?      ]   W   5          %           (           2   >       "   P      /   \   $                     9   	          <      Y       !           ^   T   
   ;       U   *   1       %s = human-readable time difference%s ago %s customer review %s customer reviews 1 Comments 404 404 - Page not Found <span>Reply Comment</span> Add a review Additional Information Author: Author: %s Average Back Be the first to review Blog Call us Cancel Reply Choose an option Clear selection Comment Comments Comments (%) Contact Contact with us Day: %s Description Edit<span class="screen-reader-text"> "%s"</span> Email Good Icon Input your key words... It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Items Leave a Reply to %s Leave a comment<span class="screen-reader-text"> on %s</span> Login Lost your password? Month: %s Name Newer Comments No media selected Not that bad Nothing Found Older Comments Only logged in customers who have purchased this product may leave a review. Oops! That page can&rsquo;t be found. Page Pages: Password Perfect Pingbacks And Trackbacks Please Wait Post Comment Product Description Product quantity input tooltipQty Rate&hellip; Rated %s out of 5 Ready to publish your first post? <a href="%1$s">Get started here</a>. Related Products Remember me Sale! Search Search Results for: %s Section Defaults Restored! Settings Saved! Sorry, but nothing matched your search terms. Please try again with some different keywords. Submit There are no reviews yet. This product is currently out of stock and unavailable. Title Upload Used before category names.Categories Used before post author name.Author Used before post format.Format Used before publish date.Posted on Used before tag names.Tags Username Username or email Very Poor View all posts by %s Website Year: %s You are customizing %s Your Comment Your Email Your Name Your Rating Your Review Your comment is awaiting moderation. based on %s customer rating based on %s customer ratings monthly archives date formatF Y out of %s5%s yearly archives date formatY Project-Id-Version: ZuraVN.Com
POT-Creation-Date: 2016-10-21 01:13+0700
PO-Revision-Date: 2016-10-21 22:25+0700
Last-Translator: 
Language-Team: 
Language: vi_VN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.4
X-Poedit-Basepath: ..
X-Poedit-WPHeader: style.css
Plural-Forms: nplurals=1; plural=0;
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-Bookmarks: -1,37,-1,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 %s về trước %s đánh giá 1 bình luận Lỗi 404 404 - Không tìm thấy trang này <span>Trả lời bình luận</span> Thêm đánh giá Thông tin thêm Người đăng Tác giả: %s Trung bình Trở lại Trở thành người đầu tiên đánh giá Blog Gọi Mua Hàng Hủy phản hồi Chọn 1 tùy chọn Xoá lựa chọn Bình luận Binh luận Bình luận (%) Liên hệ Đặt Hàng Ngày: %s Mô tả Sửa <span class="screen-reader-text"> "%s"</span> Email Tốt Icon Nhập từ khóa của bạn... Có vẻ như không có gì được thấy ở vị trí này. Thử tìm kiếm xem! Có vẻ như chúng ta không thể tìm thấy những gì bạn đang tìm kiếm. Có lẽ tìm kiếm có thể giúp đỡ. Sản phẩm Để lại bình luận cho %s Để lại một bình luận <span class="screen-reader-text">về%s</span> ĐĂNG NHẬP Quên Mật Khẩu? Tháng: %s Tên Bình luận mới nhất Không có file media nào được chọn Tạm được Không tìm thấy dữ liệu! Bình luận trước đó Chỉ đăng nhập khách hàng đã mua sản phẩm này có thể để lại một bình luận. Oops! Không tìm thấy trang Trang Trang: Mật khẩu Hoàn hảo Pingbacks và Trackbacks Vui lòng chờ Gửi phản hồi Mô tả sản phẩm Số lượng Tỷ lệ… Đánh giá%s trong 5 Bạn đã sẵn sàng để đăng bài viết đầu tiên của bạn?  <a href="%1$s">Hãy bắt đầu tại đây</a>. Sản phẩm liên quan Tự động đăng nhập Sale! Tìm kiếm Kết quả tìm kiếm: %s Mặc định của phần này đã được phục hồi! Cài đặt đã lưu! Xin lỗi, không có gì phù hợp với điều kiện tìm kiếm của bạn. Vui lòng thử lại với một số từ khóa khác nhau. Submit Hiện tại không có đánh giá nào. Sản phẩm này hiện đang hết hàng và không có sẵn. Tiêu đề Upload Danh mục Tác giả Định dạng Viết vào lúc Tags Tài khoản Tên đăng nhập hoặc Email Rất tồi Xem tất cả các bài đăng bởi %s Website Năm: %s Bạn đang tùy biến %s Bình luận của bạn Email của bạn Tên của bạn Đánh giá của bạn Đánh giá của bạn Bình luận của bạn đang chờ kiểm duyệt Dựa trên %s khách hàng đánh giá F Y trong số%s5%s Y 