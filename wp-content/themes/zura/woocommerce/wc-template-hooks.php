<?php
/**
 * WooCommerce Template Hooks
 *
 * Action/filter hooks used for WooCommerce functions/templates
 *
 * @author 		WooThemes
 * @category 	Core
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
* woocommerce_before_main_content hook
*
* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
* @hooked woocommerce_breadcrumb - 20
*/
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

//product
//remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
//remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);

//single
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}
function zura_before_recent_products_loop() {
    echo '<div class="row">';
}
add_action('woocommerce_shortcode_before_recent_products_loop', 'zura_before_recent_products_loop');
function zura_after_recent_products_loop() {
    echo '</div>';
}
add_action('woocommerce_shortcode_after_recent_products_loop', 'zura_after_recent_products_loop');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;
    ob_start();
    ?>
    <span class="cart-count">[<?php echo $woocommerce->cart->cart_contents_count; ?>]</span>
    <?php
    $fragments['span.cart-count'] = ob_get_clean();
    return $fragments;
}
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function zura_single_product_large_thumbnail_size() {
    return 'shop_catalog';
}
add_filter('single_product_large_thumbnail_size', 'zura_single_product_large_thumbnail_size');