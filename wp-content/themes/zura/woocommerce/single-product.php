<?php

/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     9.0.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header(); ?>

<?php zura_page_element(); ?>

<div class="<?php zura_main_class(); ?>">
	<div class="row">
		<section id="primary" class="col-md-8">
			<div class="inner">
        	<?php
        		/**
                 * woocommerce_before_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
        		do_action( 'woocommerce_before_main_content' );
        	?>
        		<?php while ( have_posts() ) : the_post(); ?>
        			<?php wc_get_template_part( 'content', 'single-product' ); ?>
        		<?php endwhile; // end of the loop. ?>
        	<?php
        		/**
        		 * woocommerce_after_main_content hook
        		 *
        		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
        		 */
				 
        		do_action( 'woocommerce_after_main_content' );
        	?>
			</div>
        </section><!-- #primary -->
		<section id="sidebar" class="col-md-4">
			<?php get_sidebar(); ?>
		</section><!-- #sidebar -->
	</div>
</div>

<?php get_footer(); ?>

