<?php
function zura_product_title() {
    echo sprintf('<h3 class="product-title"><a href="%s" title="%s">%s</a></h3>',
        get_the_permalink(),
        get_the_title(),
        get_the_title());
}
add_action('woocommerce_after_shop_loop_item_title', 'zura_product_title', 15);